/**
 * 
 */
package com.employee.EmployeeManagement.model;

// TODO: Auto-generated Javadoc
/**
 * The Class Employee.
 *
 * @author sony
 */
public class Employee {
	
	/** The id. */
	private Integer id;
	
	/** The name. */
	private String name;
	
	/** The address. */
	private String address;
	
	/**
	 *  The id.
	 *
	 * @return the name
	 */
	

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the address.
	 *
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * Sets the address.
	 *
	 * @param address the new address
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Instantiates a new employee.
	 */
	public Employee() {
		super();
		// TODO Auto-generated constructor stub
	}

}
