/**
 * 
 */
package com.employee.EmployeeManagement.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.employee.EmployeeManagement.model.Employee;

// TODO: Auto-generated Javadoc
/**
 * The Interface UserMapper.
 *
 * @author sony
 */
@Mapper
public interface UserMapper {

	/**
	 * Gets the all.
	 *
	 * @return the all
	 */
	@Select("select * from Employee")
	List<Employee> getAll();

	/**
	 * Gets the by id.
	 *
	 * @param id
	 *            the id
	 * @return the by id
	 */
	@Select("select * from Employee where ID=#{id}")
	Employee getById(Integer id);

	/**
	 * Insert employee.
	 *
	 * @param employee the employee
	 */
	@Insert("Insert into employee(id,name,address) values(#{id},#{name},#{address})")
	void insertEmployee(Employee employee);

	/**
	 * Delete employee.
	 *
	 * @param empId the emp id
	 */
	@Delete("DELETE FROM Employee WHERE id = #{id}")
	void deleteEmployee(Integer empId);

	/**
	 * Update employee.
	 *
	 * @param employee the employee
	 */
	@Update("Update employee set NAME=#{name}, ADDRESS=#{address} where ID=#{id}")
	void updateEmployee(Employee employee);

}
