/**
 * 
 */
package com.employee.EmployeeManagement;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.employee.EmployeeManagement.mapper.UserMapper;
import com.employee.EmployeeManagement.model.Employee;

// TODO: Auto-generated Javadoc
/**
 * The Class EmployeeController.
 *
 * @author sony
 */
@RestController
@RequestMapping("/rest/employees")

public class EmployeeController {
	
	/** The usermapper. */
	@Autowired
	private UserMapper usermapper;

	/**
	 * Gets the all.
	 *
	 * @return the all
	 */
	@GetMapping("/all")
	public List<Employee> getAll() {
		return usermapper.getAll();
	}
	
	/**
	 * Gets the by id.
	 *
	 * @param id the id
	 * @return the by id
	 */
	@GetMapping("/getemployeebyid/{id}")
	public Employee getById(@PathVariable("id") Integer id) {
		return usermapper.getById(id);
	}
	
	
	/**
	 * Insert employee.
	 *
	 * @param employee the employee
	 */
	@PostMapping("/insertemployee")
	public void insertEmployee(@RequestBody Employee employee) {
		 usermapper.insertEmployee(employee);
	}
	
	/**
	 * Delete employee.
	 *
	 * @param empId the emp id
	 */
	@DeleteMapping("/deleteemployee/{empId}")
	public void deleteEmployee(@PathVariable("empId") Integer empId){
		usermapper.deleteEmployee(empId);
	}
	
	
	/**
	 * Update employee.
	 *
	 * @param empId the emp id
	 * @param employee the employee
	 */
	@PostMapping("/updateemployee/{empId}")
	public void updateEmployee(@PathVariable("empId") Integer empId,@RequestBody Employee employee){
		usermapper.updateEmployee(employee);
	}

}
