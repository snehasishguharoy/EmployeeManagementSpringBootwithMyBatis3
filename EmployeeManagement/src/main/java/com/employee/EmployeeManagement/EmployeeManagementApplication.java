package com.employee.EmployeeManagement;

import org.apache.ibatis.type.MappedTypes;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.employee.EmployeeManagement.model.Employee;

// TODO: Auto-generated Javadoc
/**
 * The Class EmployeeManagementApplication.
 */
@MapperScan("com.employee.EmployeeManagement.mapper")
@MappedTypes(Employee.class)
@SpringBootApplication
public class EmployeeManagementApplication {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		SpringApplication.run(EmployeeManagementApplication.class, args);
	}
}
